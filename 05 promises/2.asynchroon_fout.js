const fs = require('fs');

function readJsonAsync(filename, callback){
    fs.readFile(filename, 'utf8', function (err, res){
        if (err) return callback(err);
        const wait = Math.random() * 10;
        setTimeout(function() {
            callback(null, JSON.parse(res));
        }, wait);
    });
}

readJsonAsync('test1.json', function(err, content) {
    console.log(content);
});

readJsonAsync('test2.json', function(err, content) {
    console.log(content);
});

readJsonAsync('test3.json', function(err, content) {
    console.log(content);
});

console.log('ready reading 3 files');
