const fs = require('fs');

function readJson(filename){
    return new Promise(function (fulfill, reject){
        fs.readFile(filename, 'utf8', function (err, res){
            if (err) reject(err);
            else fulfill(JSON.parse(res));
        });
    });
}

// readJson('test1.json')
//     .then(function(content) {
//         console.log(content);
//     })
//     .catch(function(error) {
//         console.log(error);
//     });

let file1 = readJson('test1.json');
let file2 = readJson('test2.json');
let file3 = readJson('test3.json');

Promise.all([file1, file2, file3])
    .then(function(contents) {
        contents.forEach(function(content) {
            console.log(content);
        });
        console.log('ready reading 3 files');
    })
    .catch(function(err) {
        console.log(err);
    });
