const fs = require('fs');

function readJsonSync(filename) {
    return JSON.parse(fs.readFileSync(filename, 'utf8'));
}

let content1 = readJsonSync('test1.json');
console.log(content1);

let content2 = readJsonSync('test2.json');
console.log(content2);

let content3 = readJsonSync('test3.json');
console.log(content3);

console.log('ready reading 3 files');
