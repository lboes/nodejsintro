const fs = require('fs');

function readJson(filename){
    return new Promise(function (fulfill, reject){
        fs.readFile(filename, 'utf8', function (err, res){
            if (err) {
                reject(err);
            } else {
                const wait = Math.random() * 1000;
                setTimeout(function() {
                    fulfill(JSON.parse(res));
                }, wait);
            }
        });
    });
}

async function readAll() {
    try {
        let file1 = await readJson('test1.json');
        let file2 = await readJson('test2.json');
        let file3 = await readJson('test3.json');
        console.log(file1);
        console.log(file2);
        console.log(file3);
    }
    catch(err) {
        console.log('Error: ', err);
    }
}

readAll().then(function() {
    console.log('ready reading 3 files');
});
