const fs = require('fs');

function readJsonAsync(filename, callback){
    fs.readFile(filename, 'utf8', function (err, res){
        if (err) return callback(err);
        callback(null, JSON.parse(res));
    });
}

readJsonAsync('test1.json', function(err, content) {
    if (err) {
        console.log(err);
    } else {
        console.log(content);
        readJsonAsync('test2.json', function(err, content) {
            if (err) {
                console.log(err);
            } else {
                console.log(content);
                readJsonAsync('test3.json', function(err, content) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(content);
                        console.log('ready reading 3 files');
                    }
                });
            }
        });
    }
});
