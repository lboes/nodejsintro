"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crypto = require("crypto");
var ALGORITHM = 'aes-256-cbc';
var generateIV = function () {
    var iv = crypto.randomBytes(16);
    return iv.toString('hex');
};
var encrypt = function (text, secret, iv) {
    if (text) {
        var ivBuffer = new Buffer(iv, 'hex');
        var cipher = crypto.createCipheriv(ALGORITHM, new Buffer(secret), ivBuffer);
        var encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return encrypted.toString('hex');
    }
    else {
        return text;
    }
};
var decrypt = function (text, secret, iv) {
    if (text) {
        var ivBuffer = new Buffer(iv, 'hex');
        var encryptedText = new Buffer(text, 'hex');
        var decipher = crypto.createDecipheriv(ALGORITHM, new Buffer(secret), ivBuffer);
        var decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
    else {
        return text;
    }
};
exports.default = {
    generateIV: generateIV,
    encrypt: encrypt,
    decrypt: decrypt
};
