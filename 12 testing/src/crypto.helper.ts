import * as crypto from 'crypto'

const ALGORITHM = 'aes-256-cbc';

let generateIV = () => {
    let iv = crypto.randomBytes(16);
    return iv.toString('hex');
};

let encrypt = (text, secret, iv) => {
    if (text) {
        let ivBuffer = new Buffer(iv, 'hex');
        let cipher = crypto.createCipheriv(ALGORITHM, new Buffer(secret), ivBuffer);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return encrypted.toString('hex');
    } else {
        return text;
    }
};

let decrypt = (text, secret, iv) => {
    if (text) {
        let ivBuffer = new Buffer(iv, 'hex');
        let encryptedText = new Buffer(text, 'hex');
        let decipher = crypto.createDecipheriv(ALGORITHM, new Buffer(secret), ivBuffer);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    } else {
        return text;
    }
};

export default {
    generateIV,
    encrypt,
    decrypt
};
