class Controller {

    constructor() {}

    static getIets(request, response) {
        let iets = Controller.doeIets();
        return response.status(200).json(iets);
    }

    private static doeIets() {
        return { result: 'iets' };
    }

}

export default Controller;
