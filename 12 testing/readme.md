Installeer TypeScript
```
sudo npm install typescript -g
```

Installeer dependencies
```
npm install
```

Build code
```
npm run build
```

Run Test
```
npm run test

```
of
```
intellij right-click Run
```
