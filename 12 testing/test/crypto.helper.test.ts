import { expect } from 'chai';
import 'mocha';
import "mocha-typescript";

import cryptoHelper from '../src/crypto.helper';

describe('crypto helper', () => {

    let SECRET = '12345678901234567890123456789012';
    const IV = cryptoHelper.generateIV();

    it('should return "null"', () =>
    {
        let doc = null;
        cryptoHelper.encrypt(doc, SECRET, IV);

        expect(doc).to.equal(null);
    });

    it('should return "{}"', () =>
    {
        let doc = {};

        let docStr = JSON.stringify(doc);
        let docEnc = cryptoHelper.encrypt(docStr, SECRET, IV);
        let docDec = cryptoHelper.decrypt(docEnc, SECRET, IV);
        let docObj = JSON.parse(docDec);

        expect(docObj).to.deep.equal({});
    });

    it('should return "[]"', () =>
    {
        let doc = [];

        let docStr = JSON.stringify(doc);
        let docEnc = cryptoHelper.encrypt(docStr, SECRET, IV);
        let docDec = cryptoHelper.decrypt(docEnc, SECRET, IV);
        let docObj = JSON.parse(docDec);

        expect(docObj).to.deep.equal([]);
    });

    it('should return "encrypt"', () =>
    {
        let doc = { id: '123456789', bla: 'abc' };

        let docStr = JSON.stringify(doc);
        let docEnc = cryptoHelper.encrypt(docStr, SECRET, IV);
        let docDec = cryptoHelper.decrypt(docEnc, SECRET, IV);
        let docObj = JSON.parse(docDec);


        expect(docObj).to.deep.equal(doc);
    });

});
