"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
require("mocha");
require("mocha-typescript");
var crypto_helper_1 = require("../src/crypto.helper");
describe('crypto helper', function () {
    var SECRET = '12345678901234567890123456789012';
    var IV = crypto_helper_1.default.generateIV();
    it('should return "null"', function () {
        var doc = null;
        crypto_helper_1.default.encrypt(doc, SECRET, IV);
        chai_1.expect(doc).to.equal(null);
    });
    it('should return "{}"', function () {
        var doc = {};
        var docStr = JSON.stringify(doc);
        var docEnc = crypto_helper_1.default.encrypt(docStr, SECRET, IV);
        var docDec = crypto_helper_1.default.decrypt(docEnc, SECRET, IV);
        var docObj = JSON.parse(docDec);
        chai_1.expect(docObj).to.deep.equal({});
    });
    it('should return "[]"', function () {
        var doc = [];
        var docStr = JSON.stringify(doc);
        var docEnc = crypto_helper_1.default.encrypt(docStr, SECRET, IV);
        var docDec = crypto_helper_1.default.decrypt(docEnc, SECRET, IV);
        var docObj = JSON.parse(docDec);
        chai_1.expect(docObj).to.deep.equal([]);
    });
    it('should return "encrypt"', function () {
        var doc = { id: '123456789', bla: 'abc' };
        var docStr = JSON.stringify(doc);
        var docEnc = crypto_helper_1.default.encrypt(docStr, SECRET, IV);
        var docDec = crypto_helper_1.default.decrypt(docEnc, SECRET, IV);
        var docObj = JSON.parse(docDec);
        chai_1.expect(docObj).to.deep.equal(doc);
    });
});
