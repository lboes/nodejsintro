Opdracht
```
Maak een chat applicatie mbv websocket/typescript.
Pas de package.json zo aan dat de juiste dependencies worden gebruikt.
Wanneer gebruiker zich voor het eerst aanmeldt, genereer een jsonwebtoken.
Bewaar de chats in een mongodb.

Als je tijd over hebt:
    Schrijf een unit of e2e test
```

Start app
```
npm start
```

In browser 
```
http://localhost:/3000
```
