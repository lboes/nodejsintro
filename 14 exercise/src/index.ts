import * as http from 'http';
import * as express from 'express';

const initHttpServer = () => {
    const host = '127.0.0.1';
    const port = 3000;

    const app = express();

    app.use(express.static(__dirname + '/public'));

    const server = http.createServer(app);
    server.listen(port, host, () => {
        console.log('HTTP Server running at http://%s:%s', server.address().address, server.address().port);
    });
};

initHttpServer();
