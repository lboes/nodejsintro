const http = require('http');
const express = require('express');

const host = '127.0.0.1';
const port = 3000;

const requestListener = (request, response) => {
    return response.status(200).json({ bla: "iets" });
};

let app = express();

app.use('/app', express.static(__dirname + '/public'));
app.get('/api', requestListener);

let server = http.createServer(app);
server.listen(port, host, () => {
    console.log('App running at http://%s:%s/app',
        server.address().address, server.address().port);
});
