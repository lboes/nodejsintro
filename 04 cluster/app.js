const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

const host = '127.0.0.1';
const port = 3000;

function requestListener(request, response) {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    response.end('hello world\n');
    console.log('process.pid', process.pid);
}

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
} else {
    let server = http.createServer(requestListener);
    server.listen(port, host, () => {
        console.log('Server running at http://%s:%s',
            server.address().address, server.address().port);
    });

    console.log(`Worker ${process.pid} started`);
}
