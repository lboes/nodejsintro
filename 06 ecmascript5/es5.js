function greetings (name) {
    return 'hello ' + name
}

var obj1 = { a: 1, b: 2 };
var obj2 = { a: 2, c: 3, d: 4};
var obj3 = Object.assign(obj1, obj2);

var obj4 = { a: 1, b: 2, c: 3 };
var a = obj4.a;
var b = obj4.b;
var c = obj4.c;

var d = 4;
var e = 4;
var f = 4;
var obj5 = { d: d, e: e, f: f };



var myModule = require('./myModule');
myModule.y();



var aGlobal;

function Es5Class() {
    this.doe = function(b) {
        aGlobal = b;
        console.log('aGlobal = ' + aGlobal);
    };
}

let k = new Es5Class();
k.doe(2);



function Num (aValue) {
    this.value = aValue;
    this.add = function(b) {
        this.value += b;
        return this;
    }

}

var num = new Num(10);
var sum = num.add(20).value;
console.log(sum);
