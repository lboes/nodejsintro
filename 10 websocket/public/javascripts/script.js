
let webSocket;

function openWebSocket() {
    webSocket = new WebSocket('ws://localhost:3010');

    webSocket.onmessage = (event) => {
        onMessage(event);
    };
}

function onMessage(event) {
    if (event.data) {
        let message = JSON.parse(event.data);
        message = message.replace(/\n/g, '<br/>');
        divMessages.innerHTML += message + "<br/><br/>";
        divMessages.scrollTop = divMessages.scrollHeight;
    }
}

function sendMessage() {
    if (txtMessage.value) {
        let utfMessage = JSON.stringify(txtMessage.value);
        webSocket.send(utfMessage);
        txtMessage.value = "";
    }
}

openWebSocket();
