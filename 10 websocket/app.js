import http from 'http';
import express from 'express';
import webSocket from 'websocket';

let USERS = [ ];

let initHttpServer = () => {
    const host = '127.0.0.1';
    const port = 3000;

    let app = express();

    app.use(express.static(__dirname + '/public'));

    let server = http.createServer(app);
    server.listen(port, host, () => {
        console.log('HTTP Server running at http://%s:%s', server.address().address, server.address().port);
    });
};

let initWebSocketServer = () => {
    let host = '127.0.0.1';
    let port = 3010;

    let server = http.createServer();

    let WebSocketServer = webSocket.server;

    new WebSocketServer({
        httpServer: server,
        autoAcceptConnections: false
    }).on('request', onRequest);

    server.listen(port, host, () => {
        console.log(`Server running at http://${server.address().address}:${server.address().port}`);
    });
};

let onRequest = (socket) => {
    let origin = socket.origin + socket.resource;

    let webSocket = socket.accept(null, origin);

    let user = USERS.find((userFind) => {
        return userFind.webSocket === webSocket;
    });
    if (!user) {
        USERS.push({ webSocket: webSocket });
    }

    webSocket.on('message', onMessage);

    webSocket.on('close', () => {
        onClose(webSocket);
    });
};

let onMessage = (utfMessage) => {
    if (utfMessage.type === 'utf8') {
        let message = JSON.parse(utfMessage.utf8Data);
        console.log('onMessage', message);
        USERS.forEach((user) => {
            sendMessage(message, user.webSocket);
        });
    } else {
        console.log('wrong message', utfMessage);
    }
};

let onClose = (webSocket) => {
    let user = USERS.find((userFind) => {
        return userFind.webSocket === webSocket;
    });
    if (user) {
        let index = USERS.indexOf(user);
        USERS.splice(index, 1);
    }
};

let sendMessage = (message, webSocket) => {
    let utfMessage = JSON.stringify(message);
    try {
        webSocket.sendUTF(utfMessage);
    } catch(error) {
        console.log('sendMessage error', error);
    }
};

initHttpServer();
initWebSocketServer();
