Installeer dependencies
```
npm install
```

Build code
```
npm run build
```

Voer code uit
```
npm start
```
