// zie: http://es6-features.org

const greetings = (name) => {
    return `hello ${name}`;
};

const obj1 = { a: 1, b: 2 };
const obj2 = { a: 2, c: 3, d: 4};
const obj3 = { ...obj1, ...obj2 };

const obj4 = { a: 1, b: 2, c: 3 };
const {
    a,
    b,
    c
} = obj4;

let d = 4;
let e = 4;
let f = 4;
let obj5 = { d, e, f };



import myModule from './myModule';
myModule.y();



const A_CONST = 7;
let aGlobal = 0;

class Es6Class {

    constructor(aParam) {
        this.memberVar = aParam;
    }

    static doeStatic(aParam = 3) {
        let a = 0;
        const x = 3;
        aGlobal = a + aParam + A_CONST + x;
        console.log(`aParam = ${aParam}`);
    };

    doeMember() {
        console.log(`this.memberVar = ${this.memberVar}`);
    }
}

Es6Class.doeStatic();
let k = new Es6Class(2);
Es6Class.doeStatic();
k.doeMember();
