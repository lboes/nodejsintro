const myModule = {
    x: 1,
    y: () => { console.log('This is ES6') }
};

export default myModule;
