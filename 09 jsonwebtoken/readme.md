Installeer dependencies
```
npm install
```

Start app
```
npm start
```

Test met postman 
```
https://www.getpostman.com/apps
```

login
```
  post:
   http://localhost:3000/login
  body:
   {
    "user": "jan",
    "password": "pwd123"
   }
```

refresh
```
  get:
   http://localhost:3000/refresh
  header:
   authorization: [token]
```

iets
```
  get:
   http://localhost:3000/iets
  header:
   authorization: [token]
```

check encode
```
https://www.base64decode.org
```
