const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const jwtController = require('./jsonwebtoken.controller');

const host = '127.0.0.1';
const port = 3000;

let app = express();
app.set('etag', false);
app.enable('trust proxy');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/login', jwtController.login);
app.get('/refresh', jwtController.refresh);
app.get('/iets', jwtController.iets);

let server = http.createServer(app);
server.listen(port, host, (error) => {
    if (!error) {
        console.log(`Server running at http://%s:%s`, server.address().address, server.address().port);
    } else {
        console.log(error);
    }
});
