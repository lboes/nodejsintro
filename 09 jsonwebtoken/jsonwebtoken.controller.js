const jwt = require('jsonwebtoken');

const TOKEN_EXPIRES_IN = 300;
const JWT_SECRET = 'top_secret_123';

module.exports.login = function(request, response) {
    checkUserAndPassword(request, response, function(user) {
        let salt = 'bla_' + Date.now();
        let token = generateToken(user, salt);
        response.status(200).json({
            token: token,
            expires: TOKEN_EXPIRES_IN
        });
    });
};

module.exports.refresh = function(request, response) {
    verifyToken(request, response, function () {
        let salt = 'gna_' + Date.now();
        let token = generateToken(request.decoded.user, salt);
        response.status(200).json({
            token: token,
            expires: TOKEN_EXPIRES_IN
        });
    });
};

module.exports.iets = function(request, response) {
    verifyToken(request, response, function () {
        response.status(200).json({
            result: 'iets'
        });
    });
};

function checkUserAndPassword(request, response, next) {
    if (request && request.body) {
        let user = request.body.user;
        let password = request.body.password;
        if (user === 'jan' && password === 'pwd123') {
            next(user);
        } else {
            response.status(403).json('Not Authorized');
        }
    } else {
        response.status(403).json('Not Authorized');
    }
}

function generateToken(user, salt) {
    return jwt.sign(
        { user: user, salt: salt },
        JWT_SECRET,
        { expiresIn: TOKEN_EXPIRES_IN }
    );
}

function verifyToken(request, response, next) {
    if (request.headers && request.headers.authorization) {
        let token = request.headers.authorization;
        jwt.verify(token, JWT_SECRET, { maxAge: TOKEN_EXPIRES_IN }, function(error, decoded) {
            if (!error && decoded) {
                request.decoded = decoded;
                next();
            } else {
                response.status(403).json('Not Authorized');
            }
        });
    } else {
        response.status(403).json('Not Authorized');
    }
}
