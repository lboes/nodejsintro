const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

function requestListener(request, response) {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    let str = 'hello world\n';
    response.end(str);
}

const server = http.createServer(requestListener);

server.listen(port, hostname, () => {
    console.log(`Server running at http://${server.address().address}:${server.address().port}`);
});
