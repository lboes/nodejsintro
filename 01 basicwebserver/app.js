const http = require('http');

const host = '127.0.0.1';
const port = 3000;

function requestListener(request, response) {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');
    response.end('hello world\n');
}

let server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log('Server running at http://%s:%s',
        server.address().address, server.address().port);
});
