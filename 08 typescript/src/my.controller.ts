class Controller {

    constructor() {}

    static getSomething(request, response) {
        let something = Controller.doSomething();
        return response.status(200).json(something);
    }

    private static doSomething() {
        return { result: 'something' };
    }

}

export default Controller;
