import * as http from 'http';
import * as express from 'express';
import myController from './my.controller';

const host = '127.0.0.1';
const port = 3000;

let app = express();
app.set('etag', false);
app.enable('trust proxy');

app.get('/', myController.getSomething);

let server = http.createServer(app);
server.listen(port, host, (error) => {
    if (!error) {
        console.log(`Server running at http://${server.address().address}:${server.address().port}`);
    } else {
        console.log(error);
    }
});
