Installeer TypeScript
```
sudo npm install typescript -g
```

Installeer dependencies
```
npm install
```

Build code
```
npm run build
```

Voer code uit
```
npm start
```

In browser
```
http://locahost:3000
```
