Installeer MongoDB op OSX
```
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/
brew update
brew install mongodb
```

Start MongoDB
```
mongod
```

Installeer Robo 3T
```
https://robomongo.org/download
```

Installeer TypeScript
```
sudo npm install typescript -g
```

Installeer dependencies
```
npm install
```

Build code
```
npm run build
```

Start application
```
npm start
```
