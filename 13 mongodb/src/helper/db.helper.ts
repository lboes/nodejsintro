import * as mongoose from 'mongoose';

class Helper {

    private static mongoUrl = 'mongodb://localhost:27017/nodeJsIntro';
    private static mongoOptions = { useMongoClient: true };

    static init() {
        (mongoose as any).Promise = global.Promise;

        mongoose.connect(this.mongoUrl, this.mongoOptions);

        mongoose.connection.once('open', () => {
            console.log('Connected to DB:', this.mongoUrl);
        });

        mongoose.connection.on('error', (err) => {
            console.error('DB error:', err);
        });
    }

}

export default Helper;
