import * as http from 'http';
import * as express from 'express';

import db from './helper/db.helper';
db.init();

import SampleModel, {ISampleDocument} from './model/sample.model';

const host = '127.0.0.1';
const port = 3000;

const app = express();
app.set('etag', false);
app.enable('trust proxy');

const server = http.createServer(app);
server.listen(port, host, (error) => {
    if (!error) {
        console.log(`Server running at http://${server.address().address}:${server.address().port}`);
    } else {
        console.error(error);
    }
});

// const sample = {
//     code: 'qwe',
//     naam: 'yy',
//     omschrijving: 'oms'
// };
//
// SampleModel.saveSample(sample, 'jan', (error, docNew) => {
//     if (!error) {
//         console.log('saved', docNew);
//     } else {
//         console.error(error);
//     }
// });

SampleModel.findSamples((error, result) => {
    if (!error) {
        console.log('samples', result);
    } else {
        console.error(error);
    }
});
