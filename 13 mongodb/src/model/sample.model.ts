import * as mongoose from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';

(mongoose as any).Promise = global.Promise;

export interface ISampleDocument extends mongoose.Document {
    code: string;
    naam: string;
    omschrijving: string;
    datumGewijzigd?: Date;
    gewijzigdDoor?: string;
}

export interface ISampleModel extends mongoose.Model<ISampleDocument> {
    findSamples(cb);
    findSample(id, cb);
    removeSample(id, cb);
    saveSample(doc, gewijzigdDoor, cb);
    preSave(doc, gewijzigdDoor);
}

export const schema = new mongoose.Schema({
    code: { type: String, required: true },
    naam: String,
    omschrijving: String,
    datumGewijzigd: { type: Date, required: true, default: Date.now },
    gewijzigdDoor: String
});

schema.plugin(uniqueValidator);

schema.static('findSamples', (cb) => {
    model.find((error, result) => {
        cb(error, result);
    });
});

schema.static('findSample', (id, cb) => {
    model.findById(id, (error, result) => {
        cb(error, result);
    });
});

schema.static('removeSample', (id, cb) => {
    model.remove({ id }, (err) => {
        cb(err);
    });
});

schema.static('saveSample', (doc, gewijzigdDoor, cb) => {
    if (!doc.id) {
        doc = new model(doc);
    }
    model.preSave(doc, gewijzigdDoor).save((err, docNew) => {
        cb(err, docNew);
    });
});

schema.static('preSave', (doc, gewijzigdDoor) => {
    doc.datumGewijzigd = Date.now();
    doc.gewijzigdDoor = gewijzigdDoor;
    return doc;
});

const model = mongoose.model<ISampleDocument, ISampleModel>('sample', schema, 'samples');

export default model;
